(function() {

    const canvHeight = 600, canvWidth = 800;

    const svg = d3.select(".class2").append("svg")
        .attr("width", canvWidth)
        .attr("height", canvHeight)
        //.style("border", "1px solid");

    const margin = {top: 50, right: 80, bottom: 50, left: 60};
    const width = canvWidth - margin.left - margin.right;
    const height = canvHeight - margin.top - margin.bottom;

    // chart title
    svg.append("text")
        .attr("id", "heading")
        .attr("y", 0)
        .attr("x", margin.left)
        .attr("dy", "1.5em")
        .attr("font-family", "sans-serif")
        .attr("font-size", "24px")
        .style("text-anchor", "left")
        .text("Crime");

    // create parent group and add left and top margin
    const g = svg.append("g")
        .attr("id", "chart-area")
        .attr("transform", "translate(" +margin.left + "," + margin.top + ")");

    // text label for the x axis
    g.append("text")
        .attr("id", "axis_labeling")
        .attr("y", height + margin.bottom / 2)
        .attr("x", width / 2)
        .attr("dy", "1em")
        .attr("font-family", "sans-serif")
        .style("text-anchor", "middle")
        .text("Year");


        //.tickValues(["A", "B", "C"])

     // text label for the y axis
    g.append("text")
        .attr("id", "axis_labeling")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - margin.left)
        .attr("x",0 - (height / 2))
        .attr("dy", "1em")
        .attr("font-family", "sans-serif")
        .style("text-anchor", "middle")
        .text("Percentage of change");


    // load the data from the cleaned csv file. 
    // note: the call is done asynchronous. 
    // That is why you have to load the data inside of a
    // callback function.
    d3.csv("./data/lineplot.csv").then(function(data) {

         // List of groups (here I have one group per column)
    var allGroup = ["totalProzent", "toetungsdelProzent", "KoerperverletzungProzent", "RaufProzent"]

        // add the options to the button
        d3.select("#selectButton")
          .selectAll('myOptions')
            .data(allGroup)
          .enter()
            .append('option')
          .text(function (d) { return d; }) // text showed in the menu
          .attr("value", function (d) { return d; }) // corresponding value returned by the button

        // A color scale: one color for each group
        var myColor = d3.scaleOrdinal()
          .domain(allGroup)
          .range(d3.schemeSet2);

        // Add X axis --> it is a date format
        var x = d3.scaleLinear()
          .domain([2010, 2016])
          .range([ 0, width ]);
        svg.append("g")
          .attr("transform", "translate(50," + (height+40) +")")
          .call(d3.axisBottom(x));

        // Add Y axis
        var y = d3.scaleLinear()
          .domain( [60, 110])
          .range([ height, 0 ]);
        svg.append("g")
          .call(d3.axisLeft(y))
          .attr("transform", "translate(50, 40)");

        // Initialize line with group a
        var line = svg
          .append('g')
          .append("path")
            .datum(data)
            .attr("d", d3.line()
              .x(function(d) { return x(+d.time) })
              .y(function(d) { return y(+d.totalProzent) })
            )
            .attr("stroke", function(d){ return myColor("totalProzent") })
            .attr("transform", "translate(50, 40)")
            .style("stroke-width", 4)
            .style("fill", "none")


        var line2 = svg
          .append('g')
          .append("path")
            .datum(data)
            .attr("d", d3.line()
              .x(function(d) { return x(+d.time) })
              .y(function(d) { return y(+d.gefuehlProzent) })
            )
            .attr("stroke", function(d){ return myColor("gefuehlProzent") })
            .attr("transform", "translate(50, 40)")
            .style("stroke-width", 4)
            .style("fill", "none")


        let legendDomain = [58, 60, 62, 64, 66, 68];
            createLegend(allGroup, myColor);

        // A function that update the chart
        function update(selectedGroup) {

          // Create new data with the selection?
          var dataFilter = data.map(function(d){return {time: d.time, value:d[selectedGroup]} })

          // Give these new data to update line
          line
              .datum(dataFilter)
              .transition()
              .duration(1000)
              .attr("d", d3.line()
                .x(function(d) { return x(+d.time) })
                .y(function(d) { return y(+d.value) })
              )
              .attr("stroke", function(d){ return myColor(selectedGroup) })
        }

        function createLegend(legendDomain, colorScale) {
        // 1. create a group to hold the legend
        const legend = svg.append("g")
            .attr("id", "legend")
            .attr("transform", "translate(" + (canvWidth - margin.right - 80) + "," + (margin.top - 20) + ")")

        // 2. create the legend boxes and the text label
        //    use .data(legendDomain) on an empty DOM selection
        const legend_entry = legend.selectAll("rect")
            .data(legendDomain)
            .enter();

        legend_entry.append("rect")
            .attr("x", 10)
            .attr("y", (d,i) => 30 * i + 30)
            .attr("width", 20)
            .attr("height", 20)
            .attr("fill", d => myColor(d))
            .attr("stroke", "black")
            .attr("stroke-width", "1");

        legend_entry.append("rect")
            .attr("x", 10)
            .attr("y", 0)
            .attr("width", 20)
            .attr("height", 20)
            .attr("fill", "#ACD555")
            .attr("stroke", "black")
            .attr("stroke-width", "1");

        legend_entry.append("text")
            .attr("x", 40)
            .attr("y", (d,i) => 30 * i + 45)
            .attr("class", "legend-text-line")
            .text(d => d);
            //.text(d => d);

        legend_entry.append("text")
            .attr("x", 40)
            .attr("y", 15)
            .attr("class", "legend-text-line")
            .text("Sicherheitsgefühl");

        // 3. create the main border of the legend
        // legend.append("rect")
        //     .attr("x", 1)
        //     .attr("y", 1)
        //     .attr("width", margin.right + 10)
        //     .attr("height", legendDomain.length * 30 + 10)
        //     .attr("fill", "none")
        //     .attr("stroke", "black")
        //     .attr("stroke-width", "1");
    }

        // When the button is changed, run the updateChart function
        d3.select("#selectButton").on("change", function(d) {
            // recover the option that has been chosen
            var selectedOption = d3.select(this).property("value")
            // run the updateChart function with this selected option
            update(selectedOption)
    })

    })


    

})();