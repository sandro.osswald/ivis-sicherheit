(function() {

    const canvHeight = 600, canvWidth = 800;

    var svg2 = d3.select(".class2").append("svg")
                                    .attr("width", canvWidth)
                                    .attr("height", canvHeight)
                                    //.style("border", "1px solid");

    const margin = {top: 50, right: 80, bottom: 50, left: 60};
    const width = canvWidth - margin.left - margin.right;
    const height = canvHeight - margin.top - margin.bottom;

    // chart title
    svg2.append("text")
        .attr("id", "heading")
        .attr("y", 0)
        .attr("x", margin.left)
        .attr("dy", "1.5em")
        .attr("font-family", "sans-serif")
        .attr("font-size", "24px")
        .style("text-anchor", "left")
        .text("Crime");

    // create parent group and add left and top margin
    const g = svg2.append("g")
        .attr("id", "chart-area")
        .attr("transform", "translate(" +margin.left + "," + margin.top + ")");

    // text label for the x axis
    g.append("text")
        .attr("id", "axis_labeling")
        .attr("y", height + margin.bottom / 2)
        .attr("x", width / 2)
        .attr("dy", "1em")
        .attr("font-family", "sans-serif")
        .style("text-anchor", "middle")
        .text("Year");


        //.tickValues(["A", "B", "C"])

     // text label for the y axis
    g.append("text")
        .attr("id", "axis_labeling")
        .attr("transform", "rotate(-90)")
        .attr("y", 0 - margin.left)
        .attr("x",0 - (height / 2))
        .attr("dy", "1em")
        .attr("font-family", "sans-serif")
        .style("text-anchor", "middle")
        .text("Percentage of change");


    // load the data from the cleaned csv file. 
    // note: the call is done asynchronous. 
    // That is why you have to load the data inside of a
    // callback function.
    d3.csv("./data/lineplot_new.csv").then(function(data) {
          
        const jahrDomain = d3.extent(data, d => Number(d.jahr));
        const yDomain = d3.extent(data, d => Number(d.jahr));


        year_data = data.filter(function(d) {
            return dateParse(d.date)
                .getFullYear() === +year;
        });

        // 1. create scales for x and y direction and for the color coding
        const x.domain(d3.extent(year_data, function(d) {
            return dateParse(d.jahr);
        }));



                    //.rangeRound([margin.left, width])
                    //.range("2011", "2012", "2013", "2014", "2015", "2016")

        const y = d3.scaleLinear()
                    .range([height + margin.top, margin.left])


        // define the 1st line
        var valueline = d3.line()
            .defined(function (d) { return d.taeterProzent !== null; })
            .x(function(d) { return x(d.jahr); })
            .y(function(d) { return y(d.taeterProzent); });

        // define the 2nd line
        var valueline2 = d3.line()
            .x(function(d) { return x(d.jahr); })
            .y(function(d) { return y(d.gefuehlProzent); });


        // Scale the range of the data
        //x.domain(d3.extent(data, function(d) { return d.jahr; }));
        y.domain([30, 10+d3.max(data, function(d) {
            return Math.max(d.taeterProzent, d.gefuehlProzent); })]);

        // 2. create and append
        //    a. x-axis
        const xAxis = d3.axisBottom(x);
        g.append("g")
            .attr("id", "x-axis")
            .attr("transform", "translate(-60, "+ height +")")
            .call(xAxis)
            //.tickValues(["A", "B", "C"]);

        //    b. y-axis
        const yAxis = d3.axisLeft(y);
        g.append("g")
            .attr("id", "y-axis")
            .attr("transform", "translate(0, -50)")
            .call(yAxis);

              // Add the valueline path.
        svg2.append("path")
            .data([data])
            .attr("class", "line")
            .attr("d", valueline);

          // Add the valueline2 path.
        svg2.append("path")
            .data([data])
            .attr("class", "line")
            .style("stroke", "red")
            .attr("d", valueline2);

    });

})();

/*

var line = d3.line()
  .defined(function (d) { return d[1] !== null; });

var data = [[0, 80], [50, 20], [100, 50], [150, 30],
  [200, 40], [250, 90], [300, null], [350, null],
  [400, 20], [450, 70], [500, 60]];

d3.select('#path1').attr('d', line(data));

*/