(function() {

    const canvHeight = 700, canvWidth = 1000;

    const svg = d3.select(".class1").append("svg")
        .attr("width", canvWidth)
        .attr("height", canvHeight)
        .style("border", "1px solid");

    // calc the width and height depending on margins.
    const margin = {top: 50, right: 80, bottom: 50, left: 60};
    const width = canvWidth - margin.left - margin.right;
    const height = canvHeight - margin.top - margin.bottom;

    // create parent group and add left and top margin
    const g = svg.append("g")
        .attr("id", "chart-area")
        .attr("transform", `translate(${margin.left},${margin.top})`);

    // chart title
    svg.append("text")
        .attr("id", "chart-title")
        .attr("y", 0)
        .attr("x", margin.left)
        .attr("dy", "1.5em")
        .text("Amount of crime and percieved security");

    //-----------------------------------------------------------
    
    const contextHolder = createContextHolder();

    // Create Event Handlers for mouse
    function mouseover(taeter, cantonId, name) {

        var toetungsdelikte = taeter[4][cantonId]
        var schwKoerperverl = taeter[6][cantonId]
        var raubProzent = taeter[16][cantonId]
        var alleStraftaten = taeter[2][cantonId]

        d3.select("#context-title").text(name);
        d3.select("#context-label1").text("alle Straftaten: " + alleStraftaten);
        d3.select("#context-label2").text("Tötungsdelikte: " + toetungsdelikte);
        d3.select("#context-label3").text("Körperverletzung: " + schwKoerperverl);
        d3.select("#context-label4").text("Raub: " + raubProzent);
        

    }

    function mouseout(taeter, cantonId) {
        contextHolder.select("path")
        d3.select("#context-title").text("");
        d3.select("#context-label1").text("");
        d3.select("#context-label2").text("");
        d3.select("#context-label3").text("");
        d3.select("#context-label4").text("");
        //country.style("fill", "white");
    }

    // create small context rectangle
    // <g id="context-holder" transform="...">
    //   <rect width="100" height="100" />
    //   <path transform="translate(50,50)"></path>
    // </g>
    function createContextHolder() {
        const contextHolder = g.append("g")
            .attr("id", "context-holder")
            .attr("transform", `translate(${width-200},${height-120})`);
        contextHolder.append("rect")
            .attr("width", 260)
            .attr("height", 160);
        contextHolder.append("path")
            .attr("transform", "translate(50,50)");
        contextHolder.append("text")
            .attr("id", "context-title")
            .attr("transform", "translate(20,20)")
            .attr("class", "titleInfobox");
        contextHolder.append("text")
            .attr("id", "context-label1")
            .attr("transform", "translate(20,50)")
            .attr("class", "textInfobox");
        contextHolder.append("text")
            .attr("id", "context-label2")
            .attr("transform", "translate(20,80)")
            .attr("class", "textInfobox");
        contextHolder.append("text")
            .attr("id", "context-label3")
            .attr("transform", "translate(20,110)")
            .attr("class", "textInfobox");
        contextHolder.append("text")
            .attr("id", "context-label4")
            .attr("transform", "translate(20,140)")
            .attr("class", "textInfobox");
        return contextHolder;
    }

    function doPlot() {
        // adapt from https://bl.ocks.org/mbostock/4207744
        var projection = d3.geoAlbers()  // Albers is best at lat 45°
            .rotate([0, 0])       // rotate around globe by lat and long
            .center([8.3, 46.8])  // lat and long in degrees
            .scale(16000)         // zoom into small switzerland, depends on the projection
            .translate([width / 2, height / 2])  // move to center of map
            .precision(.1);
        
        Promise.all([
            d3.json("/data/readme-swiss.json"),
            d3.csv("/data/swiss-cantons.csv"),
            d3.csv("/data/Taeter_2016_skaliert.csv")

        ]).then(function(data) {
            var topology = data[0];
            var citizens = data[1];
            var taeter = data[2];

            var cantons = topojson.feature(topology, topology.objects.cantons);
            console.log(taeter);


            var pathGenerator = d3.geoPath().projection(projection);
            //g.append("path")
            //    .datum(cantons)
            //    .attr("class", "canton")
            //    .attr("d", pathGenerator);

            //const colorScale = d3.scaleLinear(d3.schemeOranges[7]);


            var myColor = {
                dom: [ 0,1,2,3],
                rag: [ "#279121", "#ffd000", "#ff9502", "#d82a1e" ]
            };

            var colorScale = d3.scaleThreshold()
                                  .domain([58.0, 60.0, 62.0, 64.0, 66.0, 68.0])
                                  .range(d3.schemeRdYlGn[7]);

            var colorScaleLabel = d3.scaleThreshold()
                                    .domain([600.00, 1000.00, 1600.00])
                                    //.domain(myColor.dom)
                                    .range(myColor.rag);


            var cant = g.selectAll("path.canton")
                .data(cantons.features)
                .enter()
                .append("path")
                .attr("id", d=> d.id)
                .attr("class", "canton")
                .attr("d", pathGenerator)
                .style("fill", d=> colorScale(citizens[1][d.id]));

            
            cant.on("mouseover", d => mouseover(taeter, d.id, d.properties.name));
            cant.on("mouseout", d => mouseout(citizens.columns[0], d.id));

            g.append("path")
                .datum(topojson.mesh(topology, topology.objects.cantons))
                .attr("class", "canton-boundary")
                .attr("d", pathGenerator);


            g.selectAll("text.canton-label")
                .data(cantons.features)
                .enter().append("text")
                    .attr("class", "canton-label")
                    .attr("transform", d => "translate(" + pathGenerator.centroid(d) + ")")
                    .attr("dy", ".35em")
                    .text(d=> taeter[2][d.id]+ ", " + d.id)
                    .style("fill", d=> colorScaleLabel(taeter[2][d.id]))

            let legendDomain = [58, 60, 62, 64, 66, 68];
            createLegend(legendDomain, colorScale);
        });
    }


    function createLegend(legendDomain, colorScale) {
        // 1. create a group to hold the legend
        const legend = svg.append("g")
            .attr("id", "legend")
            .attr("transform", "translate(" + (canvWidth - margin.right - 30) + "," + (margin.top - 20) + ")")

        // 2. create the legend boxes and the text label
        //    use .data(legendDomain) on an empty DOM selection
        const legend_entry = legend.selectAll("rect")
            .data(legendDomain)
            .enter();

        legend_entry.append("rect")
            .attr("x", 10)
            .attr("y", (d,i) => 30 * i + 10)
            .attr("width", 20)
            .attr("height", 20)
            .attr("fill", d => colorScale(d))
            .attr("stroke", "black")
            .attr("stroke-width", "1");

        legend_entry.append("text")
            .attr("x", 60)
            .attr("y", (d,i) => 30 * i + 25)
            .attr("class", "legend-text-map")
            .text(d => d + " - " +(d+1.9));
            //.text(d => d);

        // 3. create the main border of the legend
        legend.append("rect")
            .attr("x", 1)
            .attr("y", 1)
            .attr("width", margin.right + 10)
            .attr("height", legendDomain.length * 30 + 10)
            .attr("fill", "none")
            .attr("stroke", "black")
            .attr("stroke-width", "1");
    }

    var tooltip = d3.select("body").append("div").classed("tooltip", true);

    

    doPlot();


})();


//# d3.interpolateRdYlGn(t) <> 
//# d3.schemeRdYlGn[k]