# $Crimes in Switzerland
**analysis between the sense of security and the amount of crimes**

Student 1: Sandro Osswald
Student 2: Cedric Bühler

**Short description:**
We analyse the connection between the sense of security and the amount of crimes once over six years (2010 - 2016 with a gab caused by missing data) and split into Cantons with a map of Switzerland.

## Comments
In our lineplot you can compare the sence of security with different crimes chosen in a dropdown menu. We choose the values from year 2010 as starting points and from there you can find the change in percent over the years (2010 till 2016).
The colors on the map from Switzerland represent the sence of security (red means people feel less safe). The color of the numbers represent the amount of crimes in relation to the inhabitants from the relative canton (red means there are a lot crimes). On an extra box you can find numbers for different kind of crimes.

## Technical information
**Main file path**: Assignment/index.html

## Status
Change to yes when you are ready.
|Version|Status|
|--|--|
|First prototype ready | yes |
|Final version ready  | no |

